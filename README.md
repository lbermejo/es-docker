

### What is this repository for? ###

* Intended to be used for Bitbucket Pipelines as a service
In `bitbucket-pipelines.yml` file:
```
definitions:
  services:
    elasticsearch:
      image: la1255/elasticsearch:temp
```


### Building
      docker build . -t la1255/es-docker:[your tag]
      docker push la1255/es-docker:[your tag]

Replace `la1255` with your account name.


### Published at
Image Names:
`la1255/elasticsearch:temp`
`la1255/elasticsearch:latest`

#### Registry:
Pushed to:
      https://hub.docker.com/r/la1255/elasticsearch/
