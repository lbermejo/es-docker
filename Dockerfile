FROM openjdk:8

# Define commonly used JAVA_HOME variable\
ENV ES_HOME /usr/local/elasticsearch
RUN apt-get update && \
  apt-get install -yq unzip wget --no-install-recommends && \
  apt-get -y autoclean

#https://download.elastic.co/elasticsearch/release/org/elasticsearch/distribution/zip/elasticsearch/2.4.0/elasticsearch-2.4.0.zip
# Install Elasticsearch.
ENV ES_PKG_NAME elasticsearch-2.4.0
RUN \
  cd / && \
  wget https://download.elasticsearch.org/elasticsearch/elasticsearch/$ES_PKG_NAME.tar.gz && \
  tar xvzf $ES_PKG_NAME.tar.gz && \
  rm -f $ES_PKG_NAME.tar.gz && \
  mv /$ES_PKG_NAME $ES_HOME && \
  chmod -R 777 $ES_HOME

COPY elasticsearch.yml /usr/share/elasticsearch/config/

EXPOSE 9200 9300 9400

#Custom user since ES is not allowed to run as root
RUN useradd --system -s /sbin/nologin daemon_user
USER daemon_user

#R un elasticsearch when this image is run
ENTRYPOINT $ES_HOME/bin/elasticsearch
